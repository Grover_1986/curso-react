import { useState } from 'react'

function Formularios() {

    // Ahora imaginemos q estamos haciendo un formulario q trata de una encuesta y hay muchas preguntas, pues esta opción no es óptima ya q tenemos muchas variables de estado.
    // Lo mejor sería tener una sóla variable de estado siempre y cuando nuestro Form sea muy grande.

    const [form, setForm] = useState({})

    /* ésta será la función q le asignaremos al evento onChange de todos los elementos del Formulario en q evaluaremos el value, q ps va a vincular alguna de las propiedades de 
    esa variable form */
    const handleChange = (e) => {
        setForm({
            /* 
            *💥 Es muy importante q los elementos del formulario tengan el atributo name
            *💥 Esto es un uso de spread syntax (...) para crear un nuevo objeto que contiene todas las propiedades del objeto form original, pero con una propiedad adicional 
                que se extrae del evento e. 
            *💥 e.target.name es el nombre del campo del formulario (como "nombre" o "correo electrónico") y e.target.value es el valor actual del campo del formulario. 
            *💥 Entonces, en esencia, estas líneas de código estás actualizando el estado del formulario con el valor del campo que ha cambiado.
            */
            ...form,
            [e.target.name]: e.target.value
        })
    }

    /* Ésta será la función q evalúe los elementos q tienen propiedad checked como el Términos y Condiciones */
    const handleChecked = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.checked
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        alert('El formulario se ha enviado')
    }

    return (
        <>
            <h2>Fomularios</h2>
            <form onSubmit={handleSubmit}>
                <label htmlFor='name'>Nombre:</label>
                {/* Aquí tenemos un input controlado mediante el estado.
                  Esta sería la forma adecuada para trabajar los input text o textarea  */}
                <input 
                    type="text" 
                    id="name" 
                    name="name" 
                    value={form.nombre} // 👈 aqui la variable nombre ahora estará dentro de objeto form
                    onChange={handleChange} 
                />

                <p>Elige tu Sabor JS Favorito:</p>
                <input 
                    type='radio' 
                    name='sabor' 
                    id='vanilla' 
                    value='vanilla'
                    onChange={handleChange} 
                    defaultChecked  //👈 con defaultChecked podemos poner x defecto un radio y hacerlo configurable, en cambio con checked no sería configurable
                />
                <label htmlFor='vanilla'>Vanilla</label>
                <br />
                <input 
                    type='radio' 
                    name='sabor' 
                    id='react' 
                    value='react'
                    onChange={handleChange} 
                />
                <label htmlFor='react'>React</label>
                <br />
                <input 
                    type='radio' 
                    name='sabor' 
                    id='angular' 
                    value='angular'
                    onChange={handleChange} 
                />
                <label htmlFor='angular'>Angular</label>
                <br />
                <input 
                    type='radio' 
                    name='sabor' 
                    id='vue' 
                    value='vue'
                    onChange={handleChange} 
                />
                <label htmlFor='vue'>Vue</label>
                <br />
                <input 
                    type='radio' 
                    name='sabor' 
                    id='svelte' 
                    value='svelte'
                    onChange={handleChange} 
                />
                <label htmlFor='svelte'>Svelte</label>

                <p>Elige tu lenguaje de programación favorito:</p>
                <select name='lenguaje' onChange={handleChange} defaultValue='php'>
                    <option value=''>---</option>
                    <option value='js'>JavaScript</option>
                    <option value='php'>PHP</option>
                    <option value='py'>Python</option>
                    <option value='go'>GO</option>
                    <option value='rb'>Ruby</option>
                </select><br /><br />

                <label htmlFor='terminos'>Acepto Términos y Condiciones</label>
                <input 
                    type='checkbox' 
                    id='terminos'
                    name='terminos' 
                    onChange={handleChecked} 
                /> <br /><br />

                <input type='submit' value='ENVIAR' /> {/* para enviar el formulario tenemos q agregar al Form el evento onSubmit y pasarle una función */}
            </form>
        </>
    )
}

export default Formularios