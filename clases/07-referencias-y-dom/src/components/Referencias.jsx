import { useRef } from "react" // importamos el hook
/**
 * Las refs y el DOM 
 *  el uso más común para una ref es acceder a un elemento DOM.
 * 
 * Cuando usar referencias ?
    Existen unos cuantos buenos casos de uso para referencias:
  👉  Controlar enfoques, selección de texto, o reproducción de medios.
  👉  Activar animaciones imperativas.
  👉  Integración con bibliotecas DOM de terceros.

 * Recapitulación
  👉  Las refs son una puerta de escape para guardar valores que no se usan en el renderizado. No las necesitarás a menudo.
  👉  Una ref es un objeto plano de JavaScript con una sola propiedad llamada current, que puedes leer o asignarle un valor.
  👉  Puedes pedirle a React que te dé una ref llamando al Hook useRef.
  👉  Como el estado, las refs retienen información entre los rerenderizados de un componente.
  👉  A diferencia del estado, asignar el valor de current de una ref no desencadena un rerenderizado.
  👉  No leas o escribas ref.current durante el renderizado. Esto hace que tu componente sea difícil de predecir.
 */

const Referencias = () => {
    let refMenu = useRef()  // esto de la referencia es como un selector q ya existe en el Dom pero dentro de React
    , refMenuBtn = useRef()
  
    const handleToggleMenu = () => {
        console.log(refMenu)
        // refMenuBtn es un object y su propiedad current permite acceder y modificar al elemento del Dom de forma segura
        if(refMenuBtn.current.textContent === 'Menú') {
            refMenuBtn.current.textContent = 'Cerrar'   // como vemos aqui se modifican sus cadenas
            refMenu.current.style.display = 'block'
        } else {
            refMenuBtn.current.textContent = 'Menú'
            refMenu.current.style.display = 'none'
        }
    }

    return (
        <>
            <h2>Referencias</h2>
            {/* aqui creamos un menú de navegación y vamos hacer q se muestre o se oculte */}
            <button onClick={handleToggleMenu} ref={refMenuBtn}>Menú</button>  {/* Usamos la referencia para asignarla a un elemento del DOM */}
            <br />
             {/* Usamos la referencia para asignarla a un elemento del DOM */}
            <nav ref={refMenu} style={{display: 'none'}}>
                <a href="#">Sección 1</a>
                <br />
                <a href="#">Sección 2</a>
                <br />
                <a href="#">Sección 3</a>
                <br />
                <a href="#">Sección 4</a>
                <br />
                <a href="#">Sección 5</a>
                <br />
            </nav>
        </>
    );
}

export default Referencias
