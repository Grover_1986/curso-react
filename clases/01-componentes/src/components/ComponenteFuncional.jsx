// este es un componente Funcional
function ComponenteFuncional (props) {
    return (
        <h2>{props.msg}</h2>
    )
}

export default ComponenteFuncional