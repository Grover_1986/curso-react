import { Component } from 'react' // destructuramos a Componente de React

// éste es un componente de Clase
class ComponenteClase extends Component  {   // extendemos 
    render() {
        // en componentes de Clase las props son un Objeto q se le pega a la this de la clase
        return <h2>{this.props.msg}</h2>
    }
}

export default ComponenteClase