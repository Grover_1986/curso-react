import ComponenteClase from './components/ComponenteClase'
import ComponenteFuncional from './components/ComponenteFuncional'
import ComponenteExpresada from './components/ComponenteExpresado'

function App() {

  return (
    <section>
      <ComponenteClase msg='Hola soy un Componente de Clase desde una prop' /> {/* llamamos a nuestra props msg */}
      <br />
      <ComponenteFuncional msg='Hola soy un Componente Funcional desde una prop' />
      <br />
      <ComponenteExpresada msg='Hola soy un Componente Expresado desde una prop' />
    </section>
  )
}

export default App
