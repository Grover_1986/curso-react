import { useState, useEffect } from 'react'

// componente Pokemon
function Pokemon({ name, link }) {
    return (
        <>
            <p>{name}</p>
            <a href={link} target='_blank'>{link}</a>
        </>
    )
}

/**
 *  Ahora usaremos fetch usando async await
 *  Aprovecharemos en ver que si se puede manejar async await dentro de un useEffect
 */
function AjaxHooks() {
    const [pokemons, setPokemons] = useState([])

    useEffect(() => { //no se antepone async en la función anonima q recibe el useEffect xq pueden suceder cosas extrañas, asi q al useEffect no lo podemos volver asíncrono

        // manera correcta de hacer peticiones asíncronas usando async await dentro de useEffect
        const getPokemons = async (url) => {
            let res = await fetch(url),
                json = await res.json()

            // aquí se acumularán todos los nuevos pokémons en el array updatedPokemons
            let updatedPokemons = [];

            /* La función forEach no espera a que las funciones asíncronas dentro de ella se completen antes de pasar a la siguiente iteración.
                * Una forma de abordar esto es usar un bucle for...of junto con async/await, ya que for...of sí espera a que las promesas se resuelvan antes de pasar a la 
                siguiente iteración. */
            for (const elem of json.results) {
                let pokemon = {
                    name: elem.name,
                    url: elem.url
                }
                updatedPokemons.push(pokemon)
            }
            /* En este código, se acumulan todos los nuevos pokémons en el array updatedPokemons, y luego se actualiza el estado una vez fuera del bucle. Esto asegura que React 
                realice una sola actualización de estado y mejora el rendimiento 
            */
            setPokemons((prevPokemons) => [...prevPokemons, ...updatedPokemons])
        }
        getPokemons('https://pokeapi.co/api/v2/pokemon')
    }, [])

    return (
        <>
            <h2>Peticiones Asíncronas con Hooks</h2>
            {pokemons.length === 0 ? (
                <h3>Cargando...</h3>
            ) : (
                pokemons.map((el, index) => (
                    <Pokemon key={index} name={el.name} link={el.url} />
                ))
            )}
        </>
    )
}

export default AjaxHooks