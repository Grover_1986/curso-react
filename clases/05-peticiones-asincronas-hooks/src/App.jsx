import AjaxHooks from './components/AjaxHooks'

function App() {

  return (
    <AjaxHooks />
  )
}

export default App
