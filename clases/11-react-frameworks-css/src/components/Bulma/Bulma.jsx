import Card from './Card';

function Bulma() {
  return (
    <div>
      <h2>Bulma</h2>
      <Card />
    </div>
  );
}

export default Bulma;
