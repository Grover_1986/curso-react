import { useEffect, useState } from "react"

function ScrollHooks() {

    const [scrollY, setScrollY] = useState(0)
    /*
       💥 useEffect recibe como parámetro una función que se ejecutará cada vez que nuestro componente se renderice, ya sea por cambios del estado o las propiedades.
       💥 Con useEffect también podemos suscribirnos y desuscribirnos a eventos, temporizadores, servicios, API's, etc.
       💥 Para ello hay que escribir el código de la suscripción en el cuerpo de la función de useEffect y para evitar problemas de rendimiento o aumento indiscriminado 
          de la memoria y recursos de nuestra aplicación retornar en una función el código que desuscriba o cancele lo que se ejecuto en el cuerpo de la función.
       💥 Podemos tener varios useEffect
       💥 Por defecto los efectos se ejecutan cada vez que se realiza un renderizado, si queremos evitar actualizaciones innecesarias o indiscriminadas podemos pasarle 
          un segundo parámetro al hook.
    */


    //✍️ El parámetro debe ser un array con todos los valores de los que dependerá el efecto, de forma que sólo se ejecutará cuando ese valor cambie.
    useEffect(() => {
        console.log('Moviendo el scroll')
        //Creamos una función para actualizar el estado
        const detectarScroll = () => setScrollY(window.pageYOffset)
        //Actualizamos el scroll al montar el componente
        detectarScroll()
        //Nos suscribimos al evento scroll de window
        window.addEventListener('scroll', detectarScroll)
        
        //✍️ La función useEffect nos permite retornar una función y cuando le decimos a un useEffect q retorne una función, internamente React se da cuenta de q queremos ejecutar
        // la fase de Desmontaje del efecto y aqui es una buena oportunidad para desuscribirnos de servicios, de APIs, limpiar intervalos de tiempo, limpiar manejadores de 
        // eventos de componentes q dejaron de existir en la UI, y todo eso nos sirve para mejorar el rendimiento de nuestra aplicación al consumir menos memoria
        // Devolvemos una función para desuscribir el evento
        return () => {
            window.removeEventListener("scroll", detectarScroll)
        }
    }, [scrollY])


    //✍️ Si le pasamos un array vacío, eso hará que el efecto no dependa de ningún valor, por lo que sólo se ejecutará al montarse y desmontarse el componente. 
    useEffect(() => {
        console.log('Fase de Montaje')
    }, [])


    //✍️ Para añadir un efecto que se ejecutará cada vez que nuestro componente se renderice, se debe pasar como parámetro una función al hook useEffect misma que se ejecutará 
    //  al renderizarse el componente.
    useEffect(() => {
        console.log('Fase de Actualización')
    })


    //✍️ La función useEffect nos permite retornar una función y cuando le decimos a un useEffect q retorne una función, internamente React se da cuenta de q queremos ejecutar
    //  la fase de Desmontaje del efecto
    useEffect(() => {
        return () => {
            console.log('Fase de Desmontaje')
        }
    })


    return (
        <>
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            <h2>Hooks - useEffect y el Ciclo de Vida</h2>
            <p>Scroll Y del navegador {scrollY}px</p>
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        </>
    )
}

export default ScrollHooks