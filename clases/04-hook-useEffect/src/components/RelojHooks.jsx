import { useState, useEffect } from 'react'

// haremos un reloj que tenga botones de Iniciar y Detener y que primero el reloj estará oculto

function Reloj({ hour }) {
    return <h3>{hour}</h3>
}

function RelojHooks() {
    const [hour, setHour] = useState(new Date().toLocaleTimeString())
    const [visible, setVisible] = useState(false)

    useEffect(() => {
        let temporizador

        if(visible) {
            temporizador = setInterval(() => {
                setHour(new Date().toLocaleTimeString())
            }, 1000);
        }else {
            clearInterval(temporizador)
        }
        // tmb desuscribimos nuestro temporizador para que no consuma memoria (mejor rendimiento)
        return () => {
            clearInterval(temporizador)
            console.log('Fase de desmontaje')
        }
    },[visible]) // cuando esta variable de estado cambie se ejecutará todo el código interno del useEffect, aqui tmb en vez de variables de estado pueden ir props

    /* 🗒️NOTA: 
    *   useEffect no tiene mucho sentido sino hay variables q controlar
    *   es mucho mejor tener useEffects distintos uno para revisar cada variable de estado q intervengan en la lógica
    */
    
    return (
        <>
            <h2>Reloj con Hooks</h2>

            {visible && <Reloj hour={hour} />}

            <button onClick={() => setVisible(true)}>Iniciar</button>
            <button onClick={() => setVisible(false)}>Detener</button>
        </>
    )
}

export default RelojHooks