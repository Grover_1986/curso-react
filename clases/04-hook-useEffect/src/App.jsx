import ScrollHooks from "./components/ScrollHooks"
import RelojHooks from "./components/RelojHooks"

function App() {

  return (
    <>
      <ScrollHooks />
      <RelojHooks />
    </>
  )
}

export default App
