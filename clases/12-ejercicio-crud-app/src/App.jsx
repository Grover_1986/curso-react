import './index.css'
import CrudApp from './components/CrudApp'

function App() {
   /**
    *💥 Este ejercicio de CRUD será falso, asi q en este 1er CRUD vamos a cargar en memoria mediante un Objeto los datos y vamos hacer las 4 operaciones pero cuando recarguemos
    * la página, los datos se perderán asi q como tarea podemos hacer que estos se guarden en localStorage
    *💥 Posteriormente haremos este mismo CRUD pero con una API falsa, esto xq en el 1er caso veremos todo lo q hemos estudiado y tmb servirá de base para luego usar Fetch
    */
   return (
      <>
         <h1>Ejercicios con React</h1>
         <CrudApp />
      </>
   )
}

export default App
