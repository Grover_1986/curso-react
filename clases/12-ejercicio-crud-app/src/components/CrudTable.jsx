import './CrudTable.css'
import CrudTableRow from "./CrudTableRow";

function CrudTable({ data, setDataToEdit, deleteData }) { // traemos las otras props

   return (
      <div>
         <h3>Tabla de Datos</h3>
         <table>
            <thead>
               <tr>
                  <th>Jugador</th>
                  <th>País</th>
                  <th>Acciones</th>
               </tr>
            </thead>
            <tbody>
               {/* esta etructura tr podriamos generarlo en otro componente */}
               {/*👇 y le aplicacamos un Renderizado Condicional */}
               {data.length === 0 ? (
                  <tr>
                     <td colSpan={3}>No hay datos</td>
                  </tr>
               ) : (
                  data.map(elem => (
                     <CrudTableRow 
                        key={elem.id}
                        elem={elem}
                        setDataToEdit={setDataToEdit} // agregamos esas props al componente hijo CrudTableRow
                        deleteData={deleteData}
                     />
                  ))
               )}
            </tbody>
         </table>
      </div>
   );
}

export default CrudTable;
