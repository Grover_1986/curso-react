import './CrudForm.css'
import { useState, useEffect } from 'react';

function CrudForm({ createData, updateData, dataToEdit, setDataToEdit }) {

   const initialForm = {
      player: '',
      country: '',
      id: null
   }

   // creamos una variable de estado para los registros pero cómo x defecto era un objeto vacío {} derrepente no se reconoce, entonces es buena práctica crear 
   // la estructura inicial 'initialForm' y pasarla
   const [form, setForm] = useState(initialForm); 

   // hemos visto q función setDataToEdit viaja hasta el componente hijo CrudTableRow y esa función actualizara la variable dataToEdit q hasta ahora no la habiamos usado
   // y la manipularemos dentro de un useEffect
   useEffect(() => {
      if(dataToEdit){ // si tiene algo
         setForm(dataToEdit)
      }else {  // sino tiene nada le damos los valores iniciales
        setForm(initialForm)
      }
   }, [dataToEdit])

   // ahora agregaremos un nuevo registro, y x ende la Tabla tmb tiene q renderizarse
   const handleChange = e => {
      setForm({
         ...form,
         [e.target.name]: e.target.value
      })
   }

   // función para controlar el submit del formulario
   const handleSubmit = e => {
      e.preventDefault()
      // válidamos para q los elementos del form no se agreguen vacíos
      if (!form.player || !form.country) {
         alert('Datos incompletos')
         return
      }
      // válidamos si existe el Id o no para pasarle su respectiva función
      if (form.id === null) {
         createData(form)
      }else {
         updateData(form)
      }
      // aquí llamamos a nuestra función para limpiar el formulario
      handleReset()
   }

   // función para el botón de limpieza
   const handleReset = e => {
      // recordemos q nuestro formulario está controlado x la variable de estado form
      setForm(initialForm)
      setDataToEdit(null)  // tmb regresamos x defecto a null la variable de estado dataToEdit 
   }

   return (
      <div>
         <h3>{dataToEdit ? 'Editar' : 'Agregar'}</h3>
         <form onSubmit={handleSubmit}>
            {/* No necesitaremos un input oculto para el Id ya q eso lo gestionaremos más adelante con una variable de estado donde almacenaremos los Ids y los registros 
            de cada jugador */}
            <input type='text' name='player' placeholder='Jugador' onChange={handleChange} value={form.player} />
            <input type='text' name='country' placeholder='País' onChange={handleChange} value={form.country} />
            <input type='submit' value='Enviar' />
            <input type='reset' value='Limpiar' onClick={handleReset} /> {/* Importante: usaremos este botón reset para limpiar el formulario ya q usaremos el mismo formulario para
             el proceso de CREATE y para el proceso de UPDATE */}
         </form>
      </div>
   );
}

export default CrudForm;
