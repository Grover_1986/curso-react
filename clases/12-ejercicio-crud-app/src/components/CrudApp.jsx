import { useEffect, useState } from 'react';
import CrudForm from './CrudForm';
import CrudTable from './CrudTable';

function CrudApp() {

   // initialDb ahora obtiene los datos de localStorage si existen, de lo contrario utiliza los datos iniciales que ya se tenían definidos.
   const initialDb = JSON.parse(localStorage.getItem('players')) || [
      {
         id: 1,
         player: 'Messi',
         country: 'Argentina'
      },
      {
         id: 2,
         player: 'Cristiano Ronaldo',
         country: 'Portugal'
      },
      {
         id: 3,
         player: 'Mbappé',
         country: 'Francia'
      },
      {
         id: 4,
         player: 'Guerrero',
         country: 'Perú'
      },
      {
         id: 5,
         player: 'Darwin Núñez',
         country: 'Uruguay'
      },
   ]

   const [db, setDb] = useState(initialDb);

   // creamos una variable de estado para saber si el estado será una función de insercción o una función de actualización
   const [dataToEdit, setDataToEdit] = useState(null)

   // creamos unas funciones para ir actualizando cada una de las peticiones al CRUD
   const createData = data => {
      // con console.log(data) nos damos cuenta que llega bien la data pero pero como es un nuevo registro su Id es null, para eso le pasamos un Date.now
      data.id = Date.now()
      setDb([
         ...db,
         data
      ])
   }

   const updateData = data => {
      // aqui hacemos un map y comparamos si el id de data q recibe es igual a alguno q tiene en sus posiciones, si es así lo modifique
      let newData = db.map(el => el.id === data.id ? data : el)
      setDb(newData)
   }

   const deleteData = id => {
      let isDelete = confirm(`¿Estás seguro de eliminar el id ${id}?`)
      if(isDelete){
         // aqui se filtra todos los registros de la base de datos que no sean igual al id de data (registro de botón eliminar)
         let newData = db.filter(el => el.id !== id) 
         setDb(newData)
      }
   }

   useEffect(() => {
      localStorage.setItem('players', JSON.stringify(db))
   }, [db])

   return (
      <div>
         <h2>CRUD App</h2>
         <CrudForm 
         // ejecución de funciones
         // para eliminar no necesitamos el form xq cada registro tiene su botón de Eliminar
            createData={createData} // {createData} es una función
            updateData={updateData} // {updateData} es una función
            dataToEdit={dataToEdit} // {dataToEdit} es un valor, esto sirve para diferenciar entre create o update
            setDataToEdit={setDataToEdit} // {setDataToEdit} es una función, esto actualizará a dataToEdit
         />
         <CrudTable 
            data={db}
            //👀IMPORTANTE: recordemos q esta es la manera en como se comunican los componentes hijos hacía los componentes padres, a traves de desencadenar eventos, justamente haremos q el 
            // {deleteData}👇 se ejecute en la Tabla para eliminar un Id
            deleteData={deleteData} // función q elimina la data de un Id
            setDataToEdit={setDataToEdit} // tmb necesitamos esta función actualizadora, esto xq al presionar el botón Editar los datos de la Tabla se tienen q ir al Form
         />
      </div>
   );
}

export default CrudApp;
