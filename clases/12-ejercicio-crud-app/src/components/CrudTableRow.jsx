function CrudTableRow({ elem, setDataToEdit, deleteData }) {
   let {player, country, id} = elem // destructuramos

   return (
      <tr>
         <td>{player}</td>
         <td>{country}</td>
         <td>
            {/* recordemos q setDataToEdit es la función q actualiza la variable de estado en el CrudApp y q cuando trae datos es una Actualización y cuando sea null 
            es una Inserción; aqui le pasamos todo el objeto elem q va formando cada fila de la tabla de jugadores */}
            <button onClick={() => setDataToEdit(elem)}>Editar</button>
            {/* y aqui pasamos la función deleteData q necesita un id y vemos como se va propagando el paso ya q esta función se encuentra en CrudApp,
            ya más adelante veremos como usar el Context q sirve para esto */}
            <button onClick={() => deleteData(id)}>Eliminar</button>
         </td>
      </tr>
   );
}

export default CrudTableRow;
