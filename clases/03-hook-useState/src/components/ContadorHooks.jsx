import { useState } from "react"

function ContadorHooks(props) { // la función tmb puede recibir propiedades
    const [contador, setContador] = useState(0) // aqui puede recibir cualquier tipo de dato: string, boolean, array, object, number

    const sumar = () => setContador(contador + 1)

    const restar = () => setContador(contador - 1)

    return (
        <>
            <h2>Hook - useState</h2>
            <nav>
                <button onClick={sumar}>+</button>
                <button onClick={restar}>-</button>
            </nav>
            <p>Contador de {props.title}</p>
            <h3>{contador}</h3>
        </>
    )
}

// tmb se pueden agregar propiedades x defecto a los componentes funcionales
ContadorHooks.defaultProps = {
    title: 'Clics'
}

export default ContadorHooks