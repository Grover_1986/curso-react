import ContadorHooks from "./components/ContadorHooks"

function App() {

  return (
    <ContadorHooks title='Seguidores' /> // aqui podemos reemplazar el valor x defecto de nuestra props title
  )
}

export default App
