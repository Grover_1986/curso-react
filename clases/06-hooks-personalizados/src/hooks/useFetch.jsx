//✍️ Si vamos a crear nuestros propios Hooks es mejor crear una carpeta con ese nombre y dentro sus respectivos archivos
//✍️ Lo q trabajaremos aquí no será un componente funcional sino una función q despues usaremos en el archivo HooksPersonalizados
//✍️ Podemos tener un Hook useFetch q haremos ahora, para peticiones Get y otro para enviar peticiones Post, Put, Delete

import { useState, useEffect } from "react"

// ahora como solo necesitamos obtener x Get la info, necesitamos la url
export const useFetch = (url) => {
    // aqui usaremos 3 variables useState
    const [data, setData] = useState(null) //👈 aqui controlaremos los datos q vienen (pokemones)
    const [isPending, setIsPending] = useState(true) //👈 variable de estado q controla cuando se ha recibido la petición y ahora esta pendiente para saber q la respuesta se envió
    const [error, setError] = useState(null)   // 👈 variable de estado para el error

    // aqui traeremos el código q hicimos en los pokemones, y este useEffect se ejecutará cuando cambie la url en el archivo donde se llamará nuestro hook custom
    useEffect(() => {
        // aqui no necesariamente es traer pokemones sino cualquier tipo de data de una api
        const getData = async (url) => {
            // aqui si usaremos el try catch
            try {
                // aqui necesitamos esperar la respuesta a la petición fetch 
                //💊 Mandamos invocar la petición
                let res = await fetch(url)
                //💊 Válidamos si hay un error
                if(!res.ok) {
                    // throw es el return de los errores, en este caso nos retorna un object de errores q va a capturarse en el catch
                    throw {
                        err: true, // esta propiedad representa el error, aqui representa q si se originó
                        status: res.status,  // este status viene del obj ajax y representa estado de la petición
                        statusText: !res.statusText ? 'Ocurrió un error' : res.statusText  // statusText es el mensaje de estadp de la API, este statusText a veces esta vacío x eso lo validamos
                    }
                }
                //💊 Si no hay un error
                let data = await res.json() // convertimos a formato json
                //💊 Actualizamos el estado de nuestras tres variables
                setIsPending(false) // false xq ya no está pendiente el resultado
                setData(data)  // actualizamos los datos q vengan en variable data
                setError({err: false}) // significa q no hubo error

                // 👇 lógica del estado q ya no va aquí xq esto es una petición generica, esto mas bien se hace en el componente q usará este Hook personalizado
                // let updatedPokemons = [];
                // for (const elem of json.results) {
                //     let pokemon = {
                //         name: elem.name,
                //         url: elem.url
                //     }
                //     updatedPokemons.push(pokemon)
                // }
                // setPokemons((prevPokemons) => [...prevPokemons, ...updatedPokemons])
            } catch (err) {
                //💊 Actualizamos variables de estado para cuando haya error
                setIsPending(true)  // sigue pendiente
                // setData(null)  el estado de los datos no se modifica xq podría modificar el estado de algún componente q lo llame 
                // y aqui podriamos resetear valores q ya habia tenido, x eso no lo modificamos
                setError(err)  // captura el error, este err es el objeto q configuramos la respuesta de petición sea falsa
            }
        }
        getData(url)

    }, [url])

    // Un hook personalizado tiene q retornar ciertos valores, puede ser un array, un object, string, number, en este caso es un object
    // como la propiedad y el valor👇 se llaman igual, se puede simplificar y llamar una vez
    return { data, isPending, error }
}