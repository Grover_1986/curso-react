import { useFetch } from '../hooks/useFetch'

function HooksPersonalizados() {
    // console.log(useFetch())
    // Aqui nuestra intencion ya no es traer los pokemones sino mostrar q si se estan recibiendo los datos
    let url = 'https://pokeapi.co/api/v2/pokemon'
    url = 'https://jsonplaceholder.typicode.com/users'

    // como useFetch devuelve un Objeto, destructuramos en formato de objeto
    let { data, isPending, error } = useFetch(url)

    // 🗒️Nota: En lugar de estar haciendo las peticiones cada vez, extraemos la funcionalidad en un archivo, retornamos los datos correspondientes y ya nomas lo  consumimos las veces
    // que lo necesitemos
 return (
    <>
        <h2>Hooks Personalizados</h2>
        {/* Para mostrar las variables tenemos q convertir a cadena de texto, xq lo q esta devolviendo el useFetch es un objeto  */}
        <h3>Pendiente: {JSON.stringify(isPending)}</h3>
        <h3>Error: <mark>{JSON.stringify(error)}</mark></h3>
        <h3>Data: <pre style={{whiteSpace: 'pre-wrap'}}><code>{JSON.stringify(data)}</code></pre></h3>
    </>
 )
}

export default HooksPersonalizados