import Propiedades from "./components/Propiedades"
import Componente from "./components/Componente"

function App() {

  return (
    <Propiedades
      cadena='Esto es una cadena de texto'
      numero={37}
      booleano={true}
      arreglo={[1,2,3,4,5,6,7]}
      objeto={{name: 'Grover', email: 'grover_cg@hotmail.com'}}
      funcion={(num) => num * num}
      elementoReact={<i>Esto es un elemento React</i>}  // esto no significa q es un html sino un elemento de React
      componenteReact={<Componente msg='Soy un componente'/>}
    />
  )
}

export default App
