import PropTypes from 'prop-types'

function Propiedades(props) {
    return (
        <ul>
            <li>{props.cadena}</li>
            <li>{props.numero}</li>
            <li>{props.booleano ? 'Verdadero' : 'Falso'}</li>
            <li>{props.arreglo.join(', ')}</li>
            <li>{`${props.objeto.name} - ${props.objeto.email}`}</li>
            <li>{props.arreglo.map(props.funcion).join(', ')}</li>
            <li>{props.elementoReact}</li>
            <li>{props.componenteReact}</li>
        </ul>
    )
}

// Los PropTypes son simplemente un mecanismo que garantiza que el valor pasado sea del tipo de datos correcto.
Propiedades.propTypes = {
    numero: PropTypes.number.isRequired // isRequired significa que la props numero tiene q ser llamada obligatoriamente en App
}

export default Propiedades