import styled, { css, keyframes, ThemeProvider, createGlobalStyle } from 'styled-components';

function ComponentesEstilizados() {
  /*
   * Esta librería Styled-Components trae integrado un wrapper de todas las etiquetas React basadas en html para generar
   * En styled.h3 estamos usando el h3 de la librería Styled-Components
   * Como Styled-Components trabaja con Template Strings, podemos agregar interpolaciones ${}
   * Tmb podemos usar el Nesting como en Sass
 */
  let mainColor = '#db7093'
    , mainAlphaColor80 = '#db709380'

  // tmb podemos crear una función y llamarla dentro de los Template Strings
  const setTranstionTime = time => `all ${time} ease-out`

  const fadeIn = keyframes`
    0% {
      opacity:0;
    }
    100% {
      opacity: 1;
    }
  `;

  const MyH3 = styled.h3`  
    padding: 2rem;
    text-align: center;
    background-color: ${mainColor};
    transition: ${setTranstionTime('1s')};
    /* color: ${props => props.color}; */
    //color: ${({ color }) => color}; /*👈 tmb se puede usar asi */
    color: ${({ color }) => color || '#fff'};  /*👈 o tmb aplicando un renderizado condicional */
    animation: ${fadeIn} .10s ease-out;

    /* como son ya varias propiedades css, usaremos la función {css} de styled-components, y esto sería como usar styled-components dentro de otro styled-components */
    ${({ isButton }) => isButton && css` 
      margin: auto;
      max-width: 50%;
      border-radius: .25rem;
      cursor: pointer;
    `} 

    &:hover {
      background-color: ${mainAlphaColor80}
    }
  `;

  const light = {
    color: '#222',
    bgColor: '#DDD'
  }

  const dark = {
    color: '#DDD',
    bgColor: '#222'
  }


  const Box = styled.div`
    padding: 1rem;
    margin: 1rem;
    color: ${({ theme }) => theme.color}; // aqui destructuramos
    background-color: ${({ theme }) => theme.bgColor};
  `;

  // Ahora imaginemos q queremos hacer una caja con bordes redondeados pero no queremos estropear nuestra caja rectangular.
  // Los Styled-Components tienen la funcionalidad q nos va a permitir heredar los estilos de componentes ya pre hechos, Veamos:
  const BoxRounded = styled(Box)` // aqui ya no usamos la notación del punto sino q ahora styled la usamos como una función y le pasamos Box para q herede sus estilos
    border-radius: 1rem;
  `; 

  const GlobalStyle = createGlobalStyle`
    h2 {
      padding: 2rem;
      background-color: #fff;
      color: #61dafb;
      text-transform: uppercase;
    }
  `;

  return (
    <>
      <GlobalStyle />    {/* como es un componente se tiene q renderizar */}
      <h2>Styled-Components</h2>
      <MyH3>Hola Soy un h3 estilizado con styled-components</MyH3>
      {/* como Styled-Components trabaja con componentes y nos permiten agregar CSS dentro de la lógica podemos pasar variables o valores como propiedades y llamarlas mas arriba
        dentro de los template strings */}
      <MyH3 color='#61dafb'>Hola Soy un h3 estilizado con styled-components</MyH3>
      <MyH3 isButton>Soy un h3 estilizado como Botón</MyH3>
      {/* Cuando usamos un tema tenemos q envolverlo y este ThemeProvider es como un contenedor q va a generar un contexto (veremos mas adelante la API de context) */}
      <ThemeProvider theme={light}> {/* este theme es como un Objeto q puede ser una serie de propiedades q vayamos definiendo */}
        <Box>Soy una caja light</Box>
        <BoxRounded>Soy una Caja Redondeada light

        </BoxRounded>
      </ThemeProvider>
      <ThemeProvider theme={dark}>
        <Box>Soy una caja dark</Box>
        <BoxRounded>Soy una Caja Redondeada dark</BoxRounded>
      </ThemeProvider>
    </>
  );
}

export default ComponentesEstilizados;
